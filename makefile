pscopy: obj/main.o
	gcc obj/main.o -o bin/pscopy

obj/main.o: src/main.c
	gcc -Wall -c src/main.c -I include/ -o obj/main.o

.PHONY: clean
clean:
	rm bin/* obj/*
