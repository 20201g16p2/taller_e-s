#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>

int main(int argc, char** argv){
//	int max = 3;
	if(argc != 3){
		printf("Los valores ingresados son incorrectos.\n");
		return -1;
	}
//	int size = 5000;
	char buff[5000] = {0};
	umask(0);
	int file = open(argv[1], O_RDONLY, 0666);
	int trunc = open(argv[2], O_TRUNC | O_CREAT | O_RDWR, 0666);
	if(file < 0){
		perror("Se produjo un error en el descriptor de archivo");
	}
	int readfile;
	int copy;
	int cont = 0;
	while((readfile = read(file,buff,5000)) != 0){
		if(readfile < 0){
			perror("Se produjo un error en la lectura del archivo");
			return -1;
		}
		copy = write(trunc,buff,readfile);
		if(copy < 0){
			perror("Se produjo un error en la escritura de la copia del archivo");
			return -1;
		}
		cont = cont + copy;
	}
	close(file);
	close(trunc);
	if(cont > 0){
		printf("%d bytes copiados.\n", cont);
	}else{
		printf("Se produjo un error en la ejecucion del programa.\n");
	}
	return 0;
}




